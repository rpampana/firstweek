function validate(number){
  let numberformat = /^0?[6-9]\d{9}$/;                //number format
  if (numberformat.test(number)){
    return "valid";                                  // if number is in given format it passes
  }
  else {
    return "invalid format";
  }
}
console.log(validate(6256123789));