function leapyear(year){
  if (year % 4 ==0 && year % 100 != 0)            // year should be divisible with 4 and should not divisible by 100
  {
    return "leap year";
  }
  else if ( year % 400 == 0)                     // year should be divisible with 400
   {
    return "leap year";
  }
  else{
    return "not a leap year";
  }
}
console.log((leapyear(1972)));