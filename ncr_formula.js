function  nCr(n,r)
{
  return fact(n) / (fact(r) * fact(n - r));               //formula for nCr
}

function fact(n){
  j = 1;
  for(i=2;i<=n;i++){                                   //number starts and get increases and multiples
    j = j*i;
  }
  return j;
}
console.log(nCr(4,3));                               // n and r values
