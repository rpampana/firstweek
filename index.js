var key=4;

switch (key) {
  case 1:
    var dictionary = require("./dictionary");
    break;
  case 2:
    var ncr = require("./ncr_formula");
    break;
  case 3:
    var repeating_letters = require("./repeating_letters");
    break;
  case 4:
    var number_square = require("./square_of_the_number")
    break;
  case 5:
    var leapyear = require("./leapyear")
    break;
  case 6:
    var multiply_number = require("./multiplty_the_number")
    break;
  case 7 :
    var power = require("./power_number")
    break;
  case 8:
    var factorial = require("./factorial")
    break;
  case 9:
    var phone_number = require("./validate_phone_number")
    break;
  case 10:
    var matrix = require("./two_dimensional_multiplication_matrix")
      default:
    break;
}